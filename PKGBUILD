# Maintainer: tioguda <guda.flavio@gmail.com>
# Contributor: Muflone http://www.muflone.com/contacts/english/

pkgname=xperia-flashtool
_realname=flashtool
pkgver=0.9.34.0
pkgrel=1
pkgdesc="A S1 protocol flashing software for Sony Xperia phones"
arch=('i686' 'x86_64')
url="http://www.flashtool.net"
license=('GPL3')
makedepends=('p7zip' 'imagemagick')
conflicts=("${pkgname}-git")
provides=("${_realname}")
#install="${pkgname}.install"
source=("https://manjariando.gitlab.io/source-packages/${pkgname}/${_realname}-${pkgver}-linux.tar.xz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname/-/_}.desktop"
        "https://metainfo.manjariando.com.br/smartphone.png"
        'https://raw.githubusercontent.com/Androxyde/Flashtool/master/LICENSE'
        "org.freedesktop.policykit.${pkgname}.policy"
        "${pkgname}.sh")
sha256sums=('c7a479fcdaef5b9e9d19e3417d72d2633befcb9e9930ff40f2bed212fe5f804d'
            '20ee6b850b5d8954c82a7298e7f491efef389b72efb0030221384bb3c394fea8'
            '5417a5e743f2847ab392eb8942eafd12aff791f2029397fe0447e5ac01c71e97'
            '3609d914b5fe0e53c772567564ea8f75e2bf3becbedd2452e26bf462dfabe67a'
            '3972dc9744f6499f0f9b2dbf76696f2ae7ad8af9b23dde66d6af86c9dfb36986'
            '4949c59138861e340473a0346997b4ecd30ea0d2962a2fd597360a6745ba5465'
            'b6b91cec623461e7b31bc3250045071350237962388ecd6df46bb437bc536803')
options=('!strip')

package() {
    depends=('libselinux' 'libsystemd' 'glib2' 'mono')

    # Install all the program files
    install -m 755 -d "${pkgdir}/usr/lib/${pkgname}"
    cp -rt "${pkgdir}/usr/lib/${pkgname}" FlashTool/*

    # Install launcher scripts
    install -m 755 -d "${pkgdir}/usr/bin"
    install -m 755 "${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
    install -Dm644 "${srcdir}/org.freedesktop.policykit.${pkgname}.policy" \
        "${pkgdir}/usr/share/polkit-1/actions/org.freedesktop.policykit.${pkgname}.policy"
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"
    install -Dm644 "${srcdir}/LICENSE" "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/smartphone.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
